This tool attempts to parse a dump of the `sec` partition from a device with a qcom SoC,
and let you know which operations it instructed the TZ to execute on first boot (the first boot
probably happened before you got the device, so this is not something you could have avoided)

To make sense of which fuses in particular were blown, you will need a register map for your SoC.
