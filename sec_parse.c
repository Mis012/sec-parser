#include <stdint.h>
#include <stdio.h>

#define BUFFER_SIZE 8192
#define SECDAT_MAGIC 0x2a126f293b7251caUL

#define ENUM_TO_STR2(val, a, b) ({ \
			char *x; \
			switch (val) { \
				case 0: x = a; break; \
				case 1: x = b; break; \
				default: x = "unknown enum value";\
			} \
			x; \
		})

#define ENUM_TO_STR10(val, a, b, c, d, e, f, g, h, i, j) ({ \
			char *x; \
			switch (val) { \
				case 0: x = a; break; \
				case 1: x = b; break; \
				case 2: x = c; break; \
				case 3: x = d; break; \
				case 4: x = e; break; \
				case 5: x = f; break; \
				case 6: x = g; break; \
				case 7: x = h; break; \
				case 8: x = i; break; \
				case 9: x = j; break; \
				default: x = "unknown enum value";\
			} \
			x; \
		})


struct fuse_arr {
	uint32_t type;
	uint32_t addr;
	uint32_t val_l;
	uint32_t val_h;
	uint32_t op;
};

int main(int argc, char **argv)
{
	uint8_t data_start[BUFFER_SIZE];
	int count;

	uint64_t magic;
	do {
		count = fread(&magic, sizeof(uint64_t), 1, stdin);
	} while (magic != SECDAT_MAGIC && count);

	if(!count) {
		fprintf(stderr, "SECDAT_MAGIC (0x%016lx) not found in the file provided\n", SECDAT_MAGIC);
		return 2;
	}

	// 8 bytes for the magic which is technically part of the data
	count = fread(data_start + 8, sizeof(uint8_t), BUFFER_SIZE - 8, stdin);

	// -- parsing starts here --

	printf("found SECDAT_MAGIC (0x%016lx)\n", magic);
	printf("!NOTICE: this tool may not be perfect, make sure to double check if something seems wrong\n");
	printf("revision: 0x%08x\n", *(uint32_t *)(data_start + 8));
	printf("unknown1: 0x%02hhx // no clue... if not 0x20 on 8998 then FAIL; is used to count total length without hash\n", *(uint8_t *)(data_start + 12));
	printf("...\n");
	printf("info string: %16s\n", data_start + 16);

	uint32_t num_seg = *(uint32_t *)(data_start + 32);
	printf("number of segments: 0x%08x\n", num_seg);
	if(num_seg == 0) {
		printf("YAY! no segments == no fuses blown (by this method anyway)\n");
		return 0;
	} else if(num_seg != 1) {
		fprintf(stderr, "number of segments is neither 0 nor 1, this is probably possible but not yet handled by this tool\n");
		return 1;
	}

	printf("segment revision: 0x%08x\n", *(uint32_t *)(data_start + 48));
	uint16_t seg_type = *(uint16_t *)(data_start + 52);
	printf("segment type: 0x%04hx (%s)\n", seg_type, ENUM_TO_STR2(seg_type, "fuse provisioning segment", "encryption key segment (?)"));
	if(seg_type != 0) {
		printf("YAY! this segment is not a fuse provisioning segment (but this probably doesn't happen when there is just one segment, so not so YAY we probably parsed something wrong)\n");
		return 1;
	}

	printf("...\n");
	printf("unknown2: 0x%08x // no clue... if not 1, FAIL\n", *(uint32_t *)(data_start + 56));
	printf("unknown3: 0x%08x // length of some sort?\n", *(uint32_t *)(data_start + 60));

	uint32_t fuse_arr_len = *(uint32_t *)(data_start + 64);
	printf("number of fuses to be blown: 0x%08x\n", fuse_arr_len);

	struct fuse_arr *fuse_array = (struct fuse_arr *)(data_start + 84);

	for(int i = 0; i < fuse_arr_len; i++) {
		printf("entry %d:\n", i);

		printf("\ttype: 0x%08x (%s)\n", fuse_array[i].type, ENUM_TO_STR10(fuse_array[i].type, "oem secure boot","oem public key hash","secure hw key","oem config","r/w permissions","spare 19","general","FEC en","anti-rollback 2","anti-rollback 3"));
		printf("\traw row address: 0x%08x\n", fuse_array[i].addr);
		printf("\tvalue to write to QFPROM_RAW_..._ROWn_LSB: 0x%08x\n", fuse_array[i].val_l);
		printf("\tvalue to write to QFPROM_RAW_..._ROWn_MSB: 0x%08x\n", fuse_array[i].val_h);
		printf("\toperation: 0x%08x (%s)\n", fuse_array[i].op, ENUM_TO_STR2(fuse_array[i].op, "blow", "verify mask (?)"));
		printf("\n");
	}

	uint8_t *sha256sum = (uint8_t *)(data_start + 84 + (fuse_arr_len * sizeof(struct fuse_arr)));
	printf("sha256sum:\n");
	printf("\t");
	for(int i = 0; i < 256 / 8; i++)
		printf("%02hhx", sha256sum[i]);
	printf("\n");

	return 0;
}
